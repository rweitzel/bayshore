# Number to Word Conversion Site


### Prerequisites

Visual Studio 2015 or newer
MVC 5

### Installing

Open CodeExercise.sln within Visual Studio 2015 or newer. After sucessfully builing the project run project and navigate to /home/index.

### Usage

To use the applicaiton simply enter a number into the text box and then hit the submit button. A box will appear with converted number in word format.

## Authors

* **Randy Weitzel** - *Initial work* - (https://bitbucket.org/rweitzel/)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
