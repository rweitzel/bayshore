﻿// actual  conversion code starts here
var ones, tens, teens

function convert_millions(num) {
    if (num >= 1000000) {
        return convert_millions(Math.floor(num / 1000000)) + " million " + convert_thousands(num % 1000000);
    }
    else {
        return convert_thousands(num);
    }
}

function convert_thousands(num) {
    if (num >= 1000) {
        return convert_hundreds(Math.floor(num / 1000)) + " thousand " + convert_hundreds(num % 1000);
    }
    else {
        return convert_hundreds(num);
    }
}

function convert_hundreds(num) {
    if (num > 99) {
        return ones[Math.floor(num / 100)] + " hundred " + convert_tens(num % 100);
    }
    else {
        return convert_tens(num);
    }
}

function convert_tens(num) {
    if (num < 10) return ones[num];
    else if (num >= 10 && num < 20) return teens[num - 10];
    else {
        return tens[Math.floor(num / 10)] + " " + ones[num % 10];
    }
}

function convert(num) {
    ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    if (num == 0) alert("zero");
    else {
        if (num % 1 == 0) {
            if (num < 0) {
                num = num * -1;

                swal("negative " + convert_millions(num)) + " dollars";
            }
            else {
                swal(convert_millions(num) + " dollars");
            }
        }
        else {
            var otherNum = round(num, 2) + '';
            var splitNum = otherNum.split(".");
            var wholeNum = splitNum[0];
            var decimalNum = splitNum[1];
            if (wholeNum == '-' || wholeNum == "-0") {
                swal("negative " + decimalNum + "/100 dollars");
            }
            else {
                if (wholeNum < 0) {
                    wholeNum = wholeNum * -1;

                    swal("negative " + convert_millions(wholeNum) + " and " + decimalNum + "/100 dollars");
                }
                else {
                    if (wholeNum == 0) {
                        swal(decimalNum + "/100 dollars");
                    }
                    else {
                        swal(convert_millions(wholeNum) + " and " + decimalNum + "/100 dollars");
                    }

                }
            }

        }
    }
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}